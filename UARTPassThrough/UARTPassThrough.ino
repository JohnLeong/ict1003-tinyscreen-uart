//-------------------------------------------------------------------------------
//  TinyCircuits ST BLE TinyShield UART Example Sketch
//  Last Updated 2 March 2016
//
//  This demo sets up the BlueNRG-MS chipset of the ST BLE module for compatiblity 
//  with Nordic's virtual UART connection, and can pass data between the Arduino
//  serial monitor and Nordic nRF UART V2.0 app or another compatible BLE
//  terminal. This example is written specifically to be fairly code compatible
//  with the Nordic NRF8001 example, with a replacement UART.ino file with
//  'aci_loop' and 'BLEsetup' functions to allow easy replacement. 
//
//  Written by Ben Rose, TinyCircuits http://tinycircuits.com
//
//-------------------------------------------------------------------------------


#include <SPI.h>
#include <STBLE.h>
#include <TinyScreen.h>
#include <string.h>
 

//#if defined(ARDUINO_ARCH_AVR)
TinyScreen display = TinyScreen(TinyScreenDefault);

//#elif defined(ARDUINO_ARCH_SAMD)
//  TinyScreen display = TinyScreen(TinyScreenPlus);

//Debug output adds extra flash and memory requirements!
#ifndef BLE_DEBUG
#define BLE_DEBUG true
#endif

#if defined (ARDUINO_ARCH_AVR)
#define SerialMonitorInterface Serial
#elif defined(ARDUINO_ARCH_SAMD)
#define SerialMonitorInterface SerialUSB
#endif



//Buttons
uint8_t buttonReleased = 1;
const uint8_t topLeftButton = TSButtonUpperLeft;
const uint8_t topRightButton = TSButtonUpperRight;
const uint8_t bottomLeftButton = TSButtonLowerLeft;
const uint8_t bottomRightButton = TSButtonLowerRight;
//BT Connection 
uint8_t ble_connection_displayed_state = true;

//Font
uint8_t defaultFontColor = TS_8b_White;
uint8_t defaultFontBG = TS_8b_Black;
const FONT_INFO& font10pt = thinPixel7_10ptFontInfo;
const FONT_INFO& font12pt = liberationSansNarrow_12ptFontInfo;
const FONT_INFO& font16pt = liberationSansNarrow_16ptFontInfo;


uint8_t ble_rx_buffer[21];
uint8_t ble_rx_buffer_len = 0;
uint8_t ble_connection_state = false;
#define PIPE_UART_OVER_BTLE_UART_TX_TX 0

uint8_t prev_answer = '0';

void setup() {
  SerialMonitorInterface.begin(9600);
  while (!SerialMonitorInterface); //This line will block until a serial monitor is opened with TinyScreen+!
  
  display.begin();
  display.setFlip(true);
  BLEsetup();
  
  
  //Start screen
  display.clearWindow(0, 12, 96, 64);
  display.setFont(font10pt);
  display.fontColor(defaultFontColor, defaultFontBG);
  display.setCursor(23, 30);
  display.print(F("ICT 1003"));
  display.setCursor(35, 40);
  display.print(F("Quiz"));
  
  
//  if(ble_connection_displayed_state == true){
//    display.setCursor(18, 20);
//    display.print(F("Start screen"));
//    display.setCursor(30, 40);
//    display.print(F("Begin!"));
//    display.setCursor(20, 55);
//    display.print(F("Connected."));
//  }
  

}


void loop() {
  aci_loop();//Process any ACI commands or events from the NRF8001- main BLE handler, must run often. Keep main loop short.
  if (ble_rx_buffer_len) {//Check if data is available
    if(ble_rx_buffer[0] == 's' || ble_rx_buffer[0] == 'S'){
      display.clearWindow(0,0,96,64);
      display.setFont(font10pt);
      display.setCursor(23, 30);
      displayscore();
    }
    if(ble_rx_buffer[0] == 'q' || ble_rx_buffer[0] == 'Q'){
      display.clearWindow(0,0,96,64);
      display.setFont(font10pt);
      display.setCursor(23, 30);
      displaymcq();
    }
    if(ble_rx_buffer[0] == 'a' || ble_rx_buffer[0] == 'A'){
      display.clearWindow(0,0,96,64);
      display.setFont(font10pt);
      display.setCursor(23, 30);
      displayanswer();
    }
    if(ble_rx_buffer[0] == 'e' || ble_rx_buffer[0] == 'E'){
      
      mainmenu();
    }
    SerialMonitorInterface.print(ble_rx_buffer_len);
    SerialMonitorInterface.print(" : ");
    SerialMonitorInterface.println((char*)ble_rx_buffer);
    
    //Read data here!
//    display.clearWindow(0, 0, 96, 64);
//    display.setFont(font16pt);
//    display.setCursor(38, 30);
//    display.print((char*)ble_rx_buffer);
    
    
    ble_rx_buffer_len = 0;//clear afer reading
  }
  if (SerialMonitorInterface.available()) {//Check if serial input is available to send
    delay(10);//should catch input
    uint8_t sendBuffer[21];
    uint8_t sendLength = 0;
    while (SerialMonitorInterface.available() && sendLength < 19) {
      sendBuffer[sendLength] = SerialMonitorInterface.read();
      sendLength++;
    }
    if (SerialMonitorInterface.available()) {
      SerialMonitorInterface.print(F("Input truncated, dropped: "));
      if (SerialMonitorInterface.available()) {
        SerialMonitorInterface.write(SerialMonitorInterface.read());
      }
    }
    sendBuffer[sendLength] = '\0'; //Terminate string
    sendLength++;
    if (!lib_aci_send_data(PIPE_UART_OVER_BTLE_UART_TX_TX, (uint8_t*)sendBuffer, sendLength))
    {
      SerialMonitorInterface.println(F("TX dropped!"));
    }
    
  }

  //Check buttons
  byte buttons = display.getButtons();
  if (buttonReleased && buttons) {
    buttonPress(buttons);
    buttonReleased = 0;
  }
  if (!buttonReleased && !(buttons & 0x0F)) {
    buttonReleased = 1;
  }
  updateBLEstatusDisplay(); 
}

//===For bluetooth connection===
void updateBLEstatusDisplay() {
  if (ble_connection_state == ble_connection_displayed_state)
    return;
  ble_connection_displayed_state = ble_connection_state;
  int x = 90;
  int y = 6;
  int s = 2;
  uint8_t color = 0xE0;
  if (ble_connection_state != true){
    color = 0x03;
    mainmenu();
  }else{
    display.setCursor(18, 20);
    display.print(F("Start screen"));
    display.setCursor(30, 40);
    display.print(F("Begin!"));
  }
  display.drawLine(x, y + s + s, x, y - s - s, color);
  display.drawLine(x - s, y + s, x + s, y - s, color);
  display.drawLine(x + s, y + s, x - s, y - s, color);
  display.drawLine(x, y + s + s, x + s, y + s, color);
  display.drawLine(x, y - s - s, x + s, y - s, color);    
}

void buttonPress(uint8_t buttons) 
{   
    delay(10);

    //This is just an example, to be removed
    if (buttons == topLeftButton)
    {
      sendStringData("A1");
      prev_answer = '1';
      //clearWindow(x start, y start, width, height);//clears specified OLED controller memory
      display.clearWindow(0, 0, 96, 64);
      display.setCursor(23, 30);
      display.print(F("Ans: A!"));
    }
    else if(buttons == topRightButton)
    {
      sendStringData("A2");
      prev_answer = '2';
      display.clearWindow(0, 0, 96, 64);
      display.setCursor(23, 30);
      display.print(F("Ans: B!"));
    }
    else if(buttons == bottomLeftButton)
    {
      sendStringData("A3");
      prev_answer = '3';
      display.clearWindow(0, 0, 96, 64);
      display.setCursor(23, 30);
      display.print(F("Ans: C!"));
    }
    else if(buttons == bottomRightButton)
    {
      sendStringData("A4");
      prev_answer = '4';
      display.clearWindow(0, 0, 96, 64);
      display.setCursor(23, 30);
      display.print(F("Ans: D!"));
    }
}

//Sends a string as an array of bytes using BLE
bool sendStringData(char *string) 
{
  if (!lib_aci_send_data(PIPE_UART_OVER_BTLE_UART_TX_TX, (uint8_t*)string, strlen(string)))
  {
    SerialMonitorInterface.println(F("TX dropped!"));
    return false;
  }
  return true;
}

//Sends an array of bytes using BLE
bool sendByteData(uint8_t *byteArray, int arrayLength)
{
  if (!lib_aci_send_data(PIPE_UART_OVER_BTLE_UART_TX_TX, byteArray, arrayLength))
  {
    SerialMonitorInterface.println(F("TX dropped!"));
    return false;
  }
  return true;
}

void displaymcq(){
  display.setCursor(0, 10); // top left
  display.print(F("A"));
  display.setCursor(90, 10); // top right
  display.print(F("B"));
  display.setCursor(0, 40); // bottom left
  display.print(F("C"));
  display.setCursor(90, 40); // bottom right
  display.print(F("D"));
}

void displayscore(){
  display.print("S: ");      
  display.print(((char*)ble_rx_buffer)[1]);
  display.print(((char*)ble_rx_buffer)[2]);
  display.print((((char*)ble_rx_buffer)[3]));
  display.print('/');  
  display.print(((char*)ble_rx_buffer)[4]);
  display.print(((char*)ble_rx_buffer)[5]);
  display.print(((char*)ble_rx_buffer)[6]);
  display.setCursor(23, 50);
  display.print("R: ");
  display.print(((char*)ble_rx_buffer)[8]);
  display.print(((char*)ble_rx_buffer)[9]);
  
  //display.print((char*)ble_rx_buffer);
}

void mainmenu(){
  display.clearWindow(0, 0, 96, 64);
  display.setFont(font10pt);
  display.fontColor(defaultFontColor, defaultFontBG);
  display.setCursor(23, 30);
  display.print(F("ICT 1003"));
  display.setCursor(35, 40);
  display.print(F("Quiz"));
}

void displayanswer(){
    display.clearWindow(0, 0, 96, 64);
    display.setFont(font16pt);
  if(prev_answer == ble_rx_buffer[1])
    display.drawRect(0, 0, 96, 64, 1, 0, 255, 0);
  else
    display.drawRect(0, 0, 96, 64, 1, 255, 0, 0);
    
  if(ble_rx_buffer[1] == '1'){
    display.setCursor(38, 30);
    display.print('A');
  }
  if(ble_rx_buffer[1] == '2'){
    display.setCursor(38, 30);
    display.print('B');
  }
  if(ble_rx_buffer[1] == '3'){
    display.setCursor(38, 30);
    display.print('C');
  }
  if(ble_rx_buffer[1] == '4'){
    display.setCursor(38, 30);
    display.print('D');
  }
  prev_answer = '0';
}
